from django.urls import path
from .views import index_view, main_view

urlpatterns = [
    path('index/', index_view, name='index_url'),
    path('', main_view, name='main_url')
]
