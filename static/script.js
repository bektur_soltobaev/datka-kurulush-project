const btn = document.querySelectorAll(".domik__btn");

const windowCont = document.querySelector(".window-cont");

const windowImg = document.querySelector(".w__img");

const esc = document.querySelector(".esc-btn");

btn.forEach(function(btn) {
  btn.addEventListener("click", function() {
    if (btn.name >= 2 && btn.name <= 11) {
      windowCont.style.display = "unset";
      console.log(btn.name);
      windowImg.setAttribute("src", btn.value);
    }
  });
});
esc.addEventListener("click", function() {
  console.log("adyl");
  windowCont.style.display = "none";
});

const slider1 = document.getElementById("myRange1");
const slider1Num = document.getElementById("slider1-num");

const slider2 = document.getElementById("myRange2");
const slider2Num = document.getElementById("slider2-num");

const area = document.querySelectorAll('input[name="area"]');

const price = document.querySelectorAll('input[name="price"]');

const totalPriceNums = document.querySelector(".total-price__nums");

let totalMoney = 0;

area.forEach(function(a) {
  a.addEventListener("click", function() {
    price.forEach(function(p) {
      p.addEventListener("click", function() {
        totalPriceNums.textContent = a.value * p.value;
        totalMoney = (a.value * p.value) / 100;
      });
    });
  });
});

price.forEach(function(p) {
  p.addEventListener("click", function() {
    area.forEach(function(a) {
      a.addEventListener("click", function() {
        totalPriceNums.textContent = a.value * p.value;
        totalMoney = (a.value * p.value) / 100;
      });
    });
  });
});

const remainedPaymentNums = document.querySelector(".remained-payment__nums");

slider1.onchange = function() {
  slider1Num.textContent = this.value + "%";
  remainedPaymentNums.textContent = Math.floor(this.value * totalMoney);
};

const totalPaymentNums = document.querySelector(".total-payment__nums");

slider2.onchange = function() {
  slider2Num.textContent = this.value + "m";
  totalPaymentNums.textContent = Math.floor(
    (totalPriceNums.textContent - remainedPaymentNums.textContent) / this.value
  );
};
